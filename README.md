# OpenML dataset: Multipurpose-World-News-Dataset

https://www.openml.org/d/43522

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This is a dataset I started building for my future personal projects, as I think this kind of data is quite hard to acquire for free and in short time. I started acquiring data on March 21st, 2020 and intend to keep doing that constantly.
What you'll have inside this are news extracted from the following sources:

Foxbusiness.com
Youtube.com
Cnet.com
The Verge
Nytimes.com
Rawstory.com
Investors.com
Wreg.com
Reuters
Koin.com
Inc.com
CNBC, Nj.com
Wmtw.com
Nbcdfw.com
Bloomberg
Wowt.com
Bbc.com

For every 20-minute interval, a script checks for new headlines on these sources and add'em into a database. This CSV file is generated from that.
I intend to update this dataset every day if I can (and if the machine I run this script is up).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43522) of an [OpenML dataset](https://www.openml.org/d/43522). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43522/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43522/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43522/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

